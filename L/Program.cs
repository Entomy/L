﻿using System;

namespace L
{
	public static class Program
	{
		public static void Main()
		{
			Console.Title = "L";
			L();
			Console.WriteLine();
			Stack.OnPop += OnPopHandler;
			Stack.OnPush += OnPushHandler;
			while (true)
			{
				Prompt().Parse().Interpret();
			}
		}

		private static void L()
		{
			Console.WriteLine("  LLLL");
			Console.WriteLine(" LLLLL");
			Console.WriteLine("LLLLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine("  LLLL");
			Console.WriteLine(" LLLLLLLLLLLLLLLLLLLL");
			Console.WriteLine("LLLLLLLLLLLLLLLLLLLL");
		}

		private static void OnPopHandler(dynamic value)
		{
			Console.WriteLine($"-> {value} < {Stack.Count}");
		}

		private static void OnPushHandler(dynamic value)
		{
			Console.WriteLine($"+> {value} < {Stack.Count}");
		}

		private static string Prompt()
		{
			Console.ForegroundColor = ConsoleColor.DarkGreen;
			Console.Write("L");
			Console.ForegroundColor = ConsoleColor.Magenta;
			Console.Write(">");
			Console.ResetColor();
			Console.Write(" ");
			return Console.ReadLine();
		}
	}
}
