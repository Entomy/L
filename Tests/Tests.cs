using Xunit;

namespace L
{
	public class Tests
	{
		public Tests()
		{
			Stack.Clear();
		}

		~Tests()
		{
			Stack.Clear();
		}

		[Theory]
		[InlineData("L", 1)]
		[InlineData("L ", 1)]
		[InlineData("LL", 2)]
		[InlineData("LL L", 2, 1)]
		[InlineData("LL L LL L", 2, 1, 2, 1)]
		[InlineData("LL L LL L LLLL", 2, 1, 2, 1, 4)]
		public void Parse(string text, params int[] opcodes)
		{
			int i = 0;
			foreach (Token token in text.Parse())
			{
				Assert.Equal(opcodes[i++], token.OpCode);
			}
		}

		[Theory]
		[InlineData("LL L", 1, 1)]
		[InlineData("LL L LL L", 2, 1)]
		[InlineData("LL L LL L LLLL", 1, 2)]
		[InlineData("LL LL LL LL LLLL", 1, 4)]
		[InlineData("LL LL LL L LLLLL", 1, 1)]
		[InlineData("LL LL LL LL LLLLLL", 1, 4)]
		[InlineData("LL LLLL LL LL LLLLLLL", 1, 2)]
		public void Parse_and_Interpret(string text, int count, dynamic top)
		{
			text.Parse().Interpret();
			Assert.Equal(count, Stack.Count);
			Assert.Equal(top, Stack.Pop());
		}
	}
}
