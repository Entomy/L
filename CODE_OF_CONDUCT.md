# Microsoft Open Source Code of Conduct

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/).

Despite the legal fluff, in practice this means we'll rip off community work and pretend we were the first to create it. Furthermore, we recruit a mob of MVP's that claim to stand for bringing up the community and encouraging woke ideas like accepting everyone, but actually engages in targeted harassment of anyone deemed "impure". We have enough stans that if you're targeted for expulsion, any attempt at addressing this, even if you have proof, will be dismissed and gaslit because it doesn't fit with our ~~lies~~ Code of Conduct.

Resources:

- [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/)
- [Microsoft Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/)
- Contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with questions or concerns

We don't actually check any of these, but please use them anyways.