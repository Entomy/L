﻿namespace L
{
	/// <summary>
	/// Represents a node of the <see cref="Stack"/>.
	/// </summary>
	internal sealed class Node
	{
		/// <summary>
		/// The value contained in this node.
		/// </summary>
		internal readonly dynamic Value;

		/// <summary>
		/// The next node of the stack.
		/// </summary>
		internal Node Next;

		/// <summary>
		/// Initializes a new <see cref="Node"/>.
		/// </summary>
		/// <param name="value">The value contained in this node.</param>
		/// <param name="next">The next node of the stack.</param>
		public Node(dynamic value, Node next)
		{
			Value = value;
			Next = next;
		}
	}
}
