﻿using System;

namespace L
{
	/// <summary>
	/// The evaluation stack.
	/// </summary>
	public static class Stack
	{
		/// <summary>
		/// The head node of the stack.
		/// </summary>
		private static Node Head = null;

		/// <summary>
		/// Event raised on each <see cref="Pop()"/>.
		/// </summary>
		public static event Action<dynamic> OnPop;

		/// <summary>
		/// Event raised on each <see cref="Push(dynamic)"/>.
		/// </summary>
		public static event Action<dynamic> OnPush;

		/// <summary>
		/// The count of values on the stack.
		/// </summary>
		public static int Count { get; private set; }

		/// <summary>
		/// Clears the stack of all values.
		/// </summary>
		public static void Clear()
		{
			while (Head is not null)
			{
				Node oldHead = Head;
				Head = Head.Next;
				oldHead.Next = null;
			}
			Count = 0;
		}

		/// <summary>
		/// Pops the top value off the stack.
		/// </summary>
		/// <returns>The value at the top of the stack.</returns>
		public static dynamic Pop()
		{
			if (Head is not null)
			{
				dynamic result = Head.Value;
				Node oldHead = Head;
				Head = Head.Next;
				oldHead.Next = null;
				Count--;
				OnPop?.Invoke(result);
				return result;
			}
			else
			{
				throw new Exception("L");
			}
		}

		/// <summary>
		/// Pushes the <paramref name="value"/> onto the stack.
		/// </summary>
		/// <param name="value">The object to push.</param>
		public static void Push(dynamic value)
		{
			Head = new(value, Head);
			Count++;
			OnPush?.Invoke(value);
		}
	}
}
