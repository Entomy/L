﻿namespace L
{
	/// <summary>
	/// Represents the <see cref="Interpreter"/> state.
	/// </summary>
	internal enum State
	{
		/// <summary>
		/// Interprets opcodes as instructions.
		/// </summary>
		Instruction = 0,

		/// <summary>
		/// Interprets opcodes as values to push.
		/// </summary>
		PushValue,
	}
}
