﻿using System;

namespace L
{
	/// <summary>
	/// Represents an <see cref="L"/> lexical token.
	/// </summary>
	public sealed class Token
	{
		/// <summary>
		/// Initializes a new <see cref="Token"/>.
		/// </summary>
		/// <param name="opcode">The OpCode of this token.</param>
		public Token(int opcode) => OpCode = opcode;

		/// <summary>
		/// Initializes a new <see cref="Token"/>.
		/// </summary>
		/// <param name="instruction">The <see cref="L.Instruction"/> of this token.</param>
		public Token(Instruction instruction) => OpCode = (int)instruction;

		/// <summary>
		/// The OpCode of this token.
		/// </summary>
		public int OpCode { get; }

		/// <summary>
		/// The Instruction of this token.
		/// </summary>
		public Instruction Instruction => (Instruction)OpCode;
	}
}
