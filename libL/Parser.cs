﻿using System;
using System.Collections.Generic;

namespace L
{
	/// <summary>
	/// Represents the <see cref="L"/> parser.
	/// </summary>
	public static class Parser
	{
		/// <summary>
		/// Parse the <paramref name="text"/>.
		/// </summary>
		/// <param name="text">The <see cref="string"/> to parse.</param>
		/// <returns>An <see cref="IEnumerable{T}"/> of parsed <see cref="Token"/>.</returns>
		public static IEnumerable<Token> Parse(this string text)
		{
			int s = 0;
			int l = 0;
			for (; l < text.Length; l++)
			{
				switch (text[l])
				{
					case 'l':
					case 'L':
						continue;
					case ' ':
					case '\t':
						yield return new(l - s);
						s = l + 1;
						break;
					default:
						throw new Exception("L");
				}
			}
			if ((l - s) > 0) yield return new(l - s);
		}
	}
}
