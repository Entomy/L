﻿using System;
using System.Collections.Generic;

namespace L
{
	/// <summary>
	/// Represents the <see cref="L"/> interpreter.
	/// </summary>
	public static class Interpreter
	{
		/// <summary>
		/// The current <see cref="L.State"/> of the <see cref="Interpreter"/>.
		/// </summary>
		private static State State { get; set; }

		/// <summary>
		/// Interprets the <paramref name="tokens"/>.
		/// </summary>
		/// <param name="tokens">The <see cref="IEnumerable{T}"/> of <see cref="Token"/> to interpret.</param>
		public static void Interpret(this IEnumerable<Token> tokens)
		{
			foreach (Token token in tokens)
			{
				switch (State)
				{
					case State.Instruction:
						InterpretInstruction(token);
						break;
					case State.PushValue:
						Stack.Push(token.OpCode);
						State = State.Instruction;
						break;
				}
			}
		}

		/// <summary>
		/// Interprets the <paramref name="token"/> as an instruction.
		/// </summary>
		/// <param name="token">The <see cref="Token"/> to interpret.</param>
		private static void InterpretInstruction(Token token)
		{
			switch (token.Instruction)
			{
				case Instruction.NoOperation:
					break;
				case Instruction.Abort:
					Console.WriteLine("   Took the L");
					Environment.Exit(0);
					break;
				case Instruction.Push:
					State = State.PushValue;
					break;
				case Instruction.Pop:
					Stack.Pop();
					break;
				case Instruction.Add:
					{
						dynamic right = Stack.Pop();
						dynamic left = Stack.Pop();
						Stack.Push(left + right);
					}
					break;
				case Instruction.Subtract:
					{
						dynamic right = Stack.Pop();
						dynamic left = Stack.Pop();
						Stack.Push(left - right);
					}
					break;
				case Instruction.Multiply:
					{
						dynamic right = Stack.Pop();
						dynamic left = Stack.Pop();
						Stack.Push(left * right);
					}
					break;
				case Instruction.Divide:
					{
						dynamic right = Stack.Pop();
						dynamic left = Stack.Pop();
						Stack.Push(left / right);
					}
					break;
				case Instruction.Negate:
					Stack.Push(-Stack.Pop());
					break;
				default:
					throw new Exception("L");
			}
		}
	}
}
