﻿namespace L
{
	/// <summary>
	/// Specifies the <see cref="Interpreter"/> instruction to execute.
	/// </summary>
	public enum Instruction
	{
		NoOperation = 0,
		Abort = 1,
		Push = 2,
		Pop = 3,
		Add = 4,
		Subtract = 5,
		Multiply = 6,
		Divide = 7,
		Negate = 8,
	}
}
